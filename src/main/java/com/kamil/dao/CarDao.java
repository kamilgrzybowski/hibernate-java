package com.kamil.dao;

import com.kamil.HibernateConfig;
import com.kamil.entity.Car;
import com.kamil.entity.CarType;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.NativeQuery;

import javax.management.Query;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class CarDao {
    private HibernateConfig hibernateConfig;


    public CarDao() {
        this.hibernateConfig = new HibernateConfig();
    }

    public Car getCar(Long id){
        Session session = hibernateConfig.getSessionFactory().openSession();
        Car car = session.get(Car.class,id);
        session.close();
        return car;
    }

    public void add(Car car){
        Session session = hibernateConfig.getSessionFactory().openSession();
        session.save(car);
        session.close();
    }

    public void updateCar(Long id, Car newCar){
        Session session = hibernateConfig.getSessionFactory().openSession();
        Transaction transaction = session.getTransaction();
        try{
            transaction.begin();
            Car car = session.find(Car.class,id);
            car.setCarType(newCar.getCarType());
            transaction.commit();
        } catch (Exception ex){
            transaction.rollback();
            ex.printStackTrace();
        } finally {
            session.close();
        }
    }


    public void deleteCar(Long id){
        Session session = hibernateConfig.getSessionFactory().openSession();
        Transaction transaction = session.getTransaction();
        try{
            transaction.begin();
            Car car = session.find(Car.class,id);
            session.delete(car);
            transaction.commit();
        } catch (Exception ex){
            transaction.rollback();
            ex.printStackTrace();
        } finally {
            session.close();
        }
    }

    public List<Car> getCarsByType(CarType carType){
        Session session = hibernateConfig.getSessionFactory().openSession();
        Transaction transaction = session.getTransaction();
        List<BigInteger> resultList = null;
        try{
            transaction.begin();
            String sql = "SELECT id FROM cars WHERE cars.car_type = ?";
            NativeQuery query = session.createNativeQuery(sql);
            query.setParameter(1,carType.ordinal());
            resultList = query.getResultList();
            transaction.commit();
        } catch (Exception ex){
            transaction.rollback();
            ex.printStackTrace();
        } finally {
            session.close();
        }

        List<Car> carList = new ArrayList<>();
        resultList.forEach(id -> carList.add(getCar(id.longValue())));
        return carList;
    }


}
