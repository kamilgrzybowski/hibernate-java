package com.kamil;

import com.kamil.dao.CarDao;
import com.kamil.entity.Car;
import com.kamil.entity.CarType;

import java.time.LocalDate;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        CarDao carDao = new CarDao();
//      Car car = new Car("Fiat", "Caro", LocalDate.of(1990,1,1), CarType.SPORT);
//      Car car1 = new Car("Fiat", "Polonez", LocalDate.of(1995,1,1), CarType.COMBI);
//      Car car2 = new Car("Volkswagen", "Polo", LocalDate.of(2000,1,1), CarType.HATCHBACK);
//      Car car3 = new Car("Opel", "Astra", LocalDate.of(2010,1,1), CarType.HATCHBACK);
//      carDao.add(car);
//      carDao.add(car1);
//      carDao.add(car2);
//      carDao.add(car3);

//      Car carReceivedFromDB = carDao.getCar(3L);
//      System.out.println(carReceivedFromDB);

//        carDao.updateCar(2L,new Car("Fiat", "Polonez", LocalDate.of(1990,1,1), CarType.COMBI));
//        Car carUpdated = carDao.getCar(2L);
//        System.out.println(carUpdated);
//        carDao.deleteCar(5L);
//
        List<Car> carsByType = carDao.getCarsByType(CarType.COMBI);
        carsByType.forEach(System.out::println);


    }
}
