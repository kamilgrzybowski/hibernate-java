package com.kamil.entity;


import javax.persistence.*;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

@Entity
@Table(name = "cars")
public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String mark;
    private String model;
    @Column(name = "production_year")
    private LocalDate prodYear;

    @Column(name = "car_type")
    @Enumerated(EnumType.ORDINAL) //changes name inside DB(String/Integer)
    private CarType carType;
    @Transient
    private long age;


    public Car() {
    }

    public Car(String mark, String model, LocalDate prodYear, CarType carType) {
        this.mark = mark;
        this.model = model;
        this.prodYear = prodYear;
        this.carType = carType;
    }

    @PostLoad
    private void calculateAge(){
        age = ChronoUnit.YEARS.between(prodYear, LocalDate.now());
    }



    public long getAge() {
        return age;
    }

    public void setAge(long age) {
        this.age = age;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public LocalDate getProdYear() {
        return prodYear;
    }

    public void setProdYear(LocalDate prodYear) {
        this.prodYear = prodYear;
    }

    public CarType getCarType() {
        return carType;
    }

    public void setCarType(CarType carType) {
        this.carType = carType;
    }

    @Override
    public String toString() {
        return "Car{" +
                "id=" + id +
                ", mark='" + mark + '\'' +
                ", model='" + model + '\'' +
                ", prodYear=" + prodYear +
                ", carType=" + carType +
                ", age=" + age +
                '}';
    }
}
