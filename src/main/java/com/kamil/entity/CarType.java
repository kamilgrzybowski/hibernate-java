package com.kamil.entity;

public enum CarType {
    SPORT, COMBI, HATCHBACK
}
